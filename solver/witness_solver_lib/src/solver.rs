use crate::{Puzzle, GridPoint, GridLine};
use crate::game_rules;
use crate::directions::*;

use std::time::{Instant, Duration};
use crate::puzzle::Vec2;

#[derive(Clone)]
pub struct Solution {
    data: Vec<Option<Direction>>,
    width: usize,
    height: usize,
    start_pos: Vec2,
}

impl Solution {
    fn new(width: usize, height: usize, start_pos: Vec2) -> Solution {
        Solution { data: vec![None; width * height], width, height, start_pos }
    }

    pub fn get(&self, pos: Vec2) -> &Option<Direction> {
        &self.data[pos.y * self.width + pos.x]
    }

    fn set(&mut self, pos: Vec2, val: Direction) {
        self.data[pos.y * self.width + pos.x] = Some(val);
    }

    fn clear(&mut self, pos: Vec2) {
        self.data[pos.y * self.width + pos.x] = None;
    }

    pub fn get_size(&self) -> Vec2 { Vec2 { x: self.width, y: self.height } }
    pub fn get_start_pos(&self) -> Vec2 { self.start_pos }
}

pub struct Solver<'a> {
    puzzle: Puzzle,
    callback_solution: &'a mut dyn FnMut(&Solution, usize),
    callback_status: &'a dyn Fn(&Solution, usize),
    callback_frequency: Duration,
}

impl Solver<'_> {
    pub fn new<'a>(puzzle: Puzzle, callback_solution: &'a mut dyn FnMut(&Solution, usize), callback_status: &'a dyn Fn(&Solution, usize), callback_frequency: Duration) -> Solver<'a> {
        Solver {
            puzzle,
            callback_solution,
            callback_status,
            callback_frequency,
        }
    }


    fn is_walkable(&self, pos: Vec2, direction: Direction) -> bool {
        match direction {
            Direction::Left => { if pos.x <= 0 { return false; } }
            Direction::Up => { if pos.y <= 0 { return false; } }
            Direction::Right => { if pos.x >= self.puzzle.get_width() { return false; } }
            Direction::Down => { if pos.y >= self.puzzle.get_height() { return false; } }
        }

        let grid_line = match direction {
            Direction::Left => self.puzzle.get_grid_line_horizontal(Vec2 { x: pos.x - 1, y: pos.y }),
            Direction::Up => self.puzzle.get_grid_line_vertical(Vec2 { x: pos.x, y: pos.y - 1 }),
            Direction::Right => self.puzzle.get_grid_line_horizontal(pos),
            Direction::Down => self.puzzle.get_grid_line_vertical(pos)
        };

        match grid_line {
            GridLine::Blocked => false,
            GridLine::Disconnected => false,
            GridLine::Normal => true,
            GridLine::Dot => true,
        }
    }

    fn solver_recursion(&mut self, current_solution: &mut Solution, pos: Vec2, previous_dir: Option<Direction>, iteration_counter: &mut usize, next_update: &mut Instant) {
        let now = Instant::now();
        if now > *next_update {
            (self.callback_status)(current_solution, *iteration_counter);
            *next_update = now + self.callback_frequency;
        }
        *iteration_counter += 1;

        // Add solution
        if let GridPoint::End = self.puzzle.get_grid_point(pos) {
            match game_rules::test(pos, &self.puzzle, current_solution) {
                Ok(()) => { (self.callback_solution)(current_solution, *iteration_counter); }
                Err(_msg) => {
                    //println!("Solution not valid: {}", msg);
                }
            }
        }

        // Iterate over directions
        for &direction in DIRECTIONS {
            // Don't walk back
            if Some(direction) == previous_dir {
                continue;
            }

            // Skip if direction is impassable
            if !self.is_walkable(pos, direction) {
                continue;
            }

            let new_pos = walk_in_direction(pos, direction);

            // Skip if we have been there already
            if current_solution.get(new_pos).is_some() {
                continue;
            }

            current_solution.set(pos, direction);
            self.solver_recursion(current_solution, new_pos, Some(anti_direction(direction)), iteration_counter, next_update);
            current_solution.clear(pos);
        }
    }

    pub fn run(&mut self) {
        let mut iteration_counter = 0;
        let mut next_update = Instant::now();

        for y in 0..=self.puzzle.get_height() {
            for x in 0..=self.puzzle.get_width() {
                if let GridPoint::Start = self.puzzle.get_grid_point(Vec2 { x, y }) {
                    let mut current_solution = Solution::new(self.puzzle.get_width() + 1, self.puzzle.get_height() + 1, Vec2 { x, y });
                    self.solver_recursion(&mut current_solution, Vec2 { x, y }, None, &mut iteration_counter, &mut next_update);
                }
            }
        }
    }
}