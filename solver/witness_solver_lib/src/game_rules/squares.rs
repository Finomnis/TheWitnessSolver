use super::group_splitting::Group;
use crate::Cell;

pub fn test(groups: &Vec<Group>) -> Result<(), &'static str> {
    for group in groups {
        let group_square_color = group.contained_cells.iter().filter_map(|(_, cell)| {
            match cell {
                Cell::Empty => None,
                Cell::Square(col) => Some(col)
            }
        }).next();

        if let Some(group_square_color) = group_square_color {
            if !group.contained_cells.iter().all(|(_, cell)| {
                match cell {
                    Cell::Empty => true,
                    Cell::Square(col) => col == group_square_color,
                }
            }) {
                return Err("Some elements don't match the square group color.");
            }
        }
    }

    Ok(())
}