mod group_splitting;
mod squares;
mod hexagons;

use crate::{Vec2, Puzzle, Solution, GridPoint};


pub fn test(end_pos: Vec2, puzzle: &Puzzle, solution: &Solution) -> Result<(), &'static str> {
    if puzzle.get_grid_point(end_pos) != &GridPoint::End {
        return Err("Solution doesn't end on an 'End' node.");
    }
    if puzzle.get_grid_point(solution.get_start_pos()) != &GridPoint::Start {
        return Err("Solution doesn't start on a 'Start' node.");
    }


    let (groups, _group_labels) = group_splitting::split_to_groups(puzzle, solution, end_pos);

    //println!("Group Labels: {:?}", group_labels);
    //println!("Groups: {:?}", groups);

    // Do the actual checking
    squares::test(&groups)?;
    hexagons::test(&groups)?;

    Ok(())
}