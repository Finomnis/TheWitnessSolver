use crate::{Vec2, Cell, Puzzle, Solution, GridLine, Direction, GridPoint};
use std::cmp;

#[derive(Clone, Debug)]
pub struct Group<'a> {
    pub id: GroupId,
    pub num_fields: usize,
    pub contained_grid_lines: Vec<&'a GridLine>,
    pub contained_grid_points: Vec<&'a GridPoint>,
    pub contained_cells: Vec<(Vec2, &'a Cell)>,
}

impl Group<'_> {
    fn new<'a>(id: GroupId) -> Group<'a> {
        Group {
            id,
            num_fields: 0,
            contained_grid_lines: vec![],
            contained_grid_points: vec![],
            contained_cells: vec![],
        }
    }
}

struct ValidConnections {
    horizontal: Vec<Vec<bool>>,
    vertical: Vec<Vec<bool>>,
}

fn find_valid_connections(solution: &Solution) -> ValidConnections {
    let size = solution.get_size();

    let mut connections = ValidConnections {
        horizontal: vec![vec![true; cmp::max(size.x, 2) - 2]; size.y - 1],
        vertical: vec![vec![true; size.x - 1]; cmp::max(size.y, 2) - 2],
    };

    let mut previous_pos = solution.get_start_pos();
    loop {
        if let Some(current_direction) = solution.get(previous_pos) {
            // Compute next position
            let next_pos = match current_direction {
                Direction::Left => Vec2 { x: previous_pos.x - 1, y: previous_pos.y },
                Direction::Right => Vec2 { x: previous_pos.x + 1, y: previous_pos.y },
                Direction::Up => Vec2 { x: previous_pos.x, y: previous_pos.y - 1 },
                Direction::Down => Vec2 { x: previous_pos.x, y: previous_pos.y + 1 },
            };

            if (previous_pos.x > 0 || next_pos.x > 0) && (previous_pos.x < size.x - 1 || next_pos.x < size.x - 1)
                && (previous_pos.y > 0 || next_pos.y > 0) && (previous_pos.y < size.y - 1 || next_pos.y < size.y - 1) {
                // Draw
                match current_direction {
                    Direction::Left | Direction::Right => {
                        connections.vertical[(previous_pos.y + next_pos.y - 2) / 2][(previous_pos.x + next_pos.x - 1) / 2] = false;
                    }
                    Direction::Up | Direction::Down => {
                        connections.horizontal[(previous_pos.y + next_pos.y - 1) / 2][(previous_pos.x + next_pos.x - 2) / 2] = false;
                    }
                }
            }

            previous_pos = next_pos;
        } else {
            break;
        }
    }

    connections
}


type GroupId = u32;

#[derive(Debug)]
pub struct GroupLabels {
    data: Vec<Option<GroupId>>,
    stride: usize,
    group_id_upper_bound: GroupId,
}

impl GroupLabels {
    fn get(&self, x: usize, y: usize) -> Option<GroupId> {
        self.data[y * self.stride + x]
    }
}

fn label_cell_groups(puzzle: &Puzzle, valid_connections: ValidConnections) -> GroupLabels {
    let width = puzzle.get_width();
    let height = puzzle.get_height();

    let mut group_labels: Vec<Option<GroupId>> = vec![None; width * height];

    let mut next_label = 1;

    for y in 0..height {
        for x in 0..width {
            // Check if cell is accessible
            if puzzle.get_grid_line_vertical(Vec2 { x, y }) == &GridLine::Disconnected
                || puzzle.get_grid_line_horizontal(Vec2 { x, y }) == &GridLine::Disconnected
                || puzzle.get_grid_line_vertical(Vec2 { x: x + 1, y }) == &GridLine::Disconnected
                || puzzle.get_grid_line_horizontal(Vec2 { x, y: y + 1 }) == &GridLine::Disconnected
            {
                continue;
            }

            let label_left = {
                if x > 0 && valid_connections.horizontal[y][x - 1] {
                    group_labels[width * y + (x - 1)]
                } else {
                    None
                }
            };

            let label_top = {
                if y > 0 && valid_connections.vertical[y - 1][x] {
                    group_labels[width * (y - 1) + x]
                } else {
                    None
                }
            };

            // Combine those two labels
            group_labels[width * y + x] = match (label_left, label_top) {
                (Some(val), None) | (None, Some(val)) => Some(val),
                (None, None) => {
                    let new_label = Some(next_label);
                    next_label += 1;
                    new_label
                }
                (Some(val1), Some(val2)) if val1 == val2 => Some(val1),
                (Some(val1), Some(val2)) => {
                    let (joined_label, obsolete_label) = if val1 < val2 { (val1, val2) } else { (val2, val1) };
                    // Modify all previous labels
                    for val in &mut group_labels[..width * y + x] {
                        if let Some(other_label) = val {
                            if *other_label == obsolete_label {
                                *other_label = joined_label;
                            }
                        }
                    }
                    // Return new label
                    Some(joined_label)
                }
            }
        }
    }

    GroupLabels { data: group_labels, stride: width, group_id_upper_bound: next_label }
}

pub(crate) fn split_to_groups<'a>(puzzle: &'a Puzzle, solution: &Solution, end_pos: Vec2) -> (Vec<Group<'a>>, GroupLabels) {
    let valid_connections = find_valid_connections(solution);

    let group_labels = label_cell_groups(puzzle, valid_connections);

    let mut groups = vec![None; group_labels.group_id_upper_bound as usize];

    // Add cells to group
    for (y, row) in puzzle.get_cells().iter().enumerate() {
        for (x, elem) in row.iter().enumerate() {
            if !match elem {
                Cell::Empty => false,
                Cell::Square(_) => true,
            } { continue; }

            if let Some(group_id) = group_labels.get(x, y) {
                let group = groups[group_id as usize].get_or_insert_with(|| { Group::new(group_id) });
                group.contained_cells.push((Vec2 { x, y }, elem));
            }
        }
    }

    for (y, row) in puzzle.get_grid_points().iter().enumerate() {
        for (x, elem) in row.iter().enumerate() {
            if !match elem {
                GridPoint::Start | GridPoint::End | GridPoint::Normal => false,
                GridPoint::Dot => true,
            } { continue; }

            // Don't process if a line is on top of the point
            if solution.get(Vec2 { x, y }).is_some() || (end_pos == Vec2 { x, y })
            {
                continue;
            }

            // Check if we are part of a group
            let mut group_ids = [(x, y), (x + 1, y), (x, y + 1), (x + 1, y + 1)]
                .iter()
                .filter_map(|&(px, py)|
                    if px <= 0 || py <= 0 || px > puzzle.get_width() || py > puzzle.get_height() {
                        None
                    } else {
                        group_labels.get(px - 1, py - 1)
                    }
                )
                .collect::<Vec<GroupId>>();
            group_ids.sort();
            group_ids.dedup();
            if group_ids.is_empty() {
                group_ids.push(0);
            }

            for group_id in group_ids {
                let group = groups[group_id as usize].get_or_insert_with(|| { Group::new(group_id) });
                group.contained_grid_points.push(elem);
            }
        }
    }


    for (y, row) in puzzle.get_grid_lines_horizontal().iter().enumerate() {
        for (x, elem) in row.iter().enumerate() {
            if !match elem {
                GridLine::Normal | GridLine::Blocked | GridLine::Disconnected => false,
                GridLine::Dot => true,
            } { continue; }

            // Don't process if a line is on top of the point
            if let &Some(Direction::Right) = solution.get(Vec2 { x, y })
            {
                continue;
            }
            if let &Some(Direction::Left) = solution.get(Vec2 { x: x + 1, y })
            {
                continue;
            }

            // Check if we are part of a group
            let mut group_ids = [(x, y), (x, y + 1)]
                .iter()
                .filter_map(|&(px, py)|
                    if py <= 0 || py > puzzle.get_height() {
                        None
                    } else {
                        group_labels.get(px, py - 1)
                    }
                )
                .collect::<Vec<GroupId>>();
            group_ids.sort();
            group_ids.dedup();
            if group_ids.is_empty() {
                group_ids.push(0);
            }

            for group_id in group_ids {
                let group = groups[group_id as usize].get_or_insert_with(|| { Group::new(group_id) });
                group.contained_grid_lines.push(elem);
            }
        }
    }

    for (y, row) in puzzle.get_grid_lines_vertical().iter().enumerate() {
        for (x, elem) in row.iter().enumerate() {
            if !match elem {
                GridLine::Normal | GridLine::Blocked | GridLine::Disconnected => false,
                GridLine::Dot => true,
            } { continue; }

            // Don't process if a line is on top of the point
            if let &Some(Direction::Down) = solution.get(Vec2 { x, y })
            {
                continue;
            }
            if let &Some(Direction::Up) = solution.get(Vec2 { x, y: y + 1 })
            {
                continue;
            }

            // Check if we are part of a group
            let mut group_ids = [(x, y), (x + 1, y)]
                .iter()
                .filter_map(|&(px, py)|
                    if px <= 0 || px > puzzle.get_width() {
                        None
                    } else {
                        group_labels.get(px - 1, py)
                    }
                )
                .collect::<Vec<GroupId>>();
            group_ids.sort();
            group_ids.dedup();
            if group_ids.is_empty() {
                group_ids.push(0);
            }

            for group_id in group_ids {
                let group = groups[group_id as usize].get_or_insert_with(|| { Group::new(group_id) });
                group.contained_grid_lines.push(elem);
            }
        }
    }
    //TODO: implement adding gridlines and gridpoints to group

    let groups_list = groups.into_iter().flatten().collect();
    (groups_list, group_labels)
}
