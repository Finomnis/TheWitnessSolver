use super::group_splitting::Group;
use crate::{GridPoint, GridLine};

pub fn test(groups: &Vec<Group>) -> Result<(), &'static str> {
    for group in groups {
        if group.contained_grid_points.iter().any(|elem| if let GridPoint::Dot = elem { true } else { false }) {
            return Err("Some hexagons on grid points are not covered!");
        }
        if group.contained_grid_lines.iter().any(|elem| if let GridLine::Dot = elem { true } else { false }) {
            return Err("Some hexagons on grid lines are not covered!");
        }
    }

    Ok(())
}