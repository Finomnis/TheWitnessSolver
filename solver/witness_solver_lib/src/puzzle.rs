use std::fmt;

#[derive(Clone, Copy, PartialEq)]
pub struct Vec2 {
    pub x: usize,
    pub y: usize,
}

impl fmt::Debug for Vec2 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[{},{}]", self.x, self.y)
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Color {
    Red,
    Green,
    Blue,
    White,
    Black,
    Cyan,
    Magenta,
    Yellow,
}

#[derive(Clone, Debug, PartialEq)]
pub enum Cell {
    Empty,
    Square(Color),
}

#[derive(Clone, Debug, PartialEq)]
pub enum GridLine {
    Normal,
    Disconnected,
    Blocked,
    Dot,
}

#[derive(Clone, Debug, PartialEq)]
pub enum GridPoint {
    Start,
    End,
    Normal,
    Dot,
}

#[derive(Debug)]
pub struct Puzzle {
    width: usize,
    height: usize,
    cells: Vec<Vec<Cell>>,
    grid_lines_horizontal: Vec<Vec<GridLine>>,
    grid_lines_vertical: Vec<Vec<GridLine>>,
    grid_points: Vec<Vec<GridPoint>>,
}

impl Puzzle {
    pub fn new(width: usize, height: usize) -> Puzzle {
        Puzzle {
            width,
            height,
            cells: vec![vec![Cell::Empty; width]; height],
            grid_lines_horizontal: vec![vec![GridLine::Normal; width]; height + 1],
            grid_lines_vertical: vec![vec![GridLine::Normal; width + 1]; height],
            grid_points: vec![vec![GridPoint::Normal; width + 1]; height + 1],
        }
    }

    pub fn set_grid_point(&mut self, pos: Vec2, val: GridPoint) {
        assert!(pos.x < self.width + 1);
        assert!(pos.y < self.height + 1);
        self.grid_points[pos.y][pos.x] = val;
    }

    pub fn set_grid_line_vertical(&mut self, pos: Vec2, val: GridLine) {
        assert!(pos.x < self.width + 1);
        assert!(pos.y < self.height);
        self.grid_lines_vertical[pos.y][pos.x] = val;
    }
    pub fn set_grid_line_horizontal(&mut self, pos: Vec2, val: GridLine) {
        assert!(pos.x < self.width);
        assert!(pos.y < self.height + 1);
        self.grid_lines_horizontal[pos.y][pos.x] = val;
    }
    pub fn set_cell(&mut self, pos: Vec2, val: Cell) {
        assert!(pos.x < self.width);
        assert!(pos.y < self.height);
        self.cells[pos.y][pos.x] = val;
    }

    pub fn get_grid_point(&self, pos: Vec2) -> &GridPoint {
        assert!(pos.x < self.width + 1);
        assert!(pos.y < self.height + 1);
        &self.grid_points[pos.y][pos.x]
    }

    pub fn get_grid_line_vertical(&self, pos: Vec2) -> &GridLine {
        assert!(pos.x < self.width + 1);
        assert!(pos.y < self.height);
        &self.grid_lines_vertical[pos.y][pos.x]
    }

    pub fn get_grid_line_horizontal(&self, pos: Vec2) -> &GridLine {
        assert!(pos.x < self.width);
        assert!(pos.y < self.height + 1);
        &self.grid_lines_horizontal[pos.y][pos.x]
    }

    pub fn get_cell(&self, pos: Vec2) -> &Cell {
        assert!(pos.x < self.width);
        assert!(pos.y < self.height);
        &self.cells[pos.y][pos.x]
    }

    pub fn get_width(&self) -> usize { self.width }
    pub fn get_height(&self) -> usize { self.height }

    pub fn get_grid_lines_horizontal(&self) -> &Vec<Vec<GridLine>> { &self.grid_lines_horizontal }
    pub fn get_grid_lines_vertical(&self) -> &Vec<Vec<GridLine>> { &self.grid_lines_vertical }
    pub fn get_grid_points(&self) -> &Vec<Vec<GridPoint>> { &self.grid_points }
    pub fn get_cells(&self) -> &Vec<Vec<Cell>> { &self.cells }
}
