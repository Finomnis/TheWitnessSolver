use crate::Vec2;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

pub fn anti_direction(dir: Direction) -> Direction {
    match dir {
        Direction::Down => Direction::Up,
        Direction::Up => Direction::Down,
        Direction::Right => Direction::Left,
        Direction::Left => Direction::Right,
    }
}

pub fn walk_in_direction(pos: Vec2, dir: Direction) -> Vec2 {
    match dir {
        Direction::Up => Vec2 { x: pos.x, y: pos.y - 1 },
        Direction::Down => Vec2 { x: pos.x, y: pos.y + 1 },
        Direction::Left => Vec2 { x: pos.x - 1, y: pos.y },
        Direction::Right => Vec2 { x: pos.x + 1, y: pos.y },
    }
}

pub const DIRECTIONS: &[Direction; 4] = &[Direction::Up, Direction::Right, Direction::Down, Direction::Left];
