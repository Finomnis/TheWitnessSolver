/*
- Don't check at the end, instead:
    - Game starts with one big area
    - Whenever a wall gets touched, the area gets split in two and we can perform a validity check on the cut off area

- Create Stepper class
    - can do step()->Incomplete,Uncompletable,Finished
    - can do undo()

- Use larger field that encodes all the entire field with one data structure. This enables flood fill and other neat features

- When you reach the wall, you _always_ have 2 possible ways to go. The other two are: the wall, and where you came from.
- Only do a check when you hit a wall head on. Otherwise, you just slide along the wall without actually splitting anything.
- ALWAYS check both directions as soon as you touch a wall.
    - Check both pieces for: - if cutting off that piece would loose the game
                             - if entering the piece would still be winnable (might not have an end node)
            Both of those properties can be checked in the same flood fill, which halves the evaluation time

            Two phases.
                - First phase:
                    - collect all items of the side
                    - count the number of tiles, as a first check for the tiling pieces
                    - check for existance of exit

                - Already rule out the impossible decisions for reasons of exits
                - Enter all possible tiles, use previous result to do a more in-depth check of the cut-off side




- Problems: Not always does hitting a wall mean cutting the game in two pieces.
    -> Solution: Enumerate Islands! As soon as we touch an island, it becomes a valid wall.
    -> Store state of the islands in a Vec<bool>.

- Implement separate step for exit the game

- DON'T just simply track the cursor, instead render it directly into the map. Store the starting point and directions in an extra vec, for final result.

- BASICALLY rewrite most of it. Keep: - Game. Also rewrite solution to be a vector of directions only

- Also, add a cool visualization that increases/decreases in brightness, depending on how many paths run over a specific tile
- Use the background color for console for solution
- make tiles 5x3 instead of 2x1

*/