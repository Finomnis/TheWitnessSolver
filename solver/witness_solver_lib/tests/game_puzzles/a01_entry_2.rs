use crate::common::test_solve_puzzle;
use witness_solver_lib as solver;
use witness_solver_lib::GridPoint::{Start, End};
use witness_solver_lib::GridPoint;
use witness_solver_lib::GridLine;
use witness_solver_lib::Vec2;


#[test]
fn puzzle_01() {
    let mut puzzle = solver::Puzzle::new(2, 2);
    puzzle.set_grid_point(Vec2 { y: 0, x: 0 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 2, x: 2 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 2, x: 0 }, Start);
    puzzle.set_grid_point(Vec2 { y: 0, x: 2 }, End);
    test_solve_puzzle(puzzle, 2);
}

#[test]
fn puzzle_02() {
    let mut puzzle = solver::Puzzle::new(3, 3);
    puzzle.set_grid_line_vertical(Vec2 { y: 0, x: 0 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 0, x: 0 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 1, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_point(Vec2 { y: 0, x: 1 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 1, x: 2 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 2, x: 0 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 2, x: 1 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 3, x: 3 }, GridPoint::Dot);

    puzzle.set_grid_point(Vec2 { y: 3, x: 0 }, Start);
    puzzle.set_grid_point(Vec2 { y: 0, x: 3 }, End);
    test_solve_puzzle(puzzle, 2);
}

#[test]
fn puzzle_03() {
    let mut puzzle = solver::Puzzle::new(3, 3);
    puzzle.set_grid_line_vertical(Vec2 { y: 0, x: 0 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 0, x: 0 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 1, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 1, x: 1 }, GridLine::Disconnected);
    puzzle.set_grid_point(Vec2 { y: 0, x: 1 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 1, x: 2 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 2, x: 0 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 2, x: 1 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 3, x: 3 }, GridPoint::Dot);

    puzzle.set_grid_point(Vec2 { y: 3, x: 0 }, Start);
    puzzle.set_grid_point(Vec2 { y: 0, x: 3 }, End);
    test_solve_puzzle(puzzle, 1);
}

#[test]
fn puzzle_04() {
    let mut puzzle = solver::Puzzle::new(3, 3);
    puzzle.set_grid_line_vertical(Vec2 { y: 0, x: 0 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 0, x: 0 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 1, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_point(Vec2 { y: 0, x: 1 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 1, x: 2 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 1, x: 3 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 2, x: 0 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 2, x: 1 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 3, x: 3 }, GridPoint::Dot);

    puzzle.set_grid_point(Vec2 { y: 1, x: 1 }, Start);
    puzzle.set_grid_point(Vec2 { y: 2, x: 2 }, Start);
    puzzle.set_grid_point(Vec2 { y: 1, x: 0 }, End);
    test_solve_puzzle(puzzle, 1);
}

#[test]
fn puzzle_05() {
    let mut puzzle = solver::Puzzle::new(3, 3);
    puzzle.set_grid_line_vertical(Vec2 { y: 0, x: 0 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 0, x: 0 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 1, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_point(Vec2 { y: 0, x: 1 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 1, x: 2 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 1, x: 3 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 2, x: 0 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 2, x: 1 }, GridPoint::Dot);
    puzzle.set_grid_point(Vec2 { y: 3, x: 3 }, GridPoint::Dot);

    puzzle.set_grid_point(Vec2 { y: 0, x: 2 }, Start);
    puzzle.set_grid_point(Vec2 { y: 0, x: 3 }, Start);
    puzzle.set_grid_point(Vec2 { y: 2, x: 3 }, Start);
    puzzle.set_grid_point(Vec2 { y: 1, x: 0 }, End);

    test_solve_puzzle(puzzle, 1);
}
