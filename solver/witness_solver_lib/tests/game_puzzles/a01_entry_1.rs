use witness_solver_lib as solver;
use witness_solver_lib::GridLine;
use witness_solver_lib::GridPoint::{Start, End};
use witness_solver_lib::Vec2;
use crate::common::test_solve_puzzle;

#[test]
fn puzzle_00() {
    let mut puzzle = solver::Puzzle::new(0, 1);
    puzzle.set_grid_point(solver::Vec2 { x: 0, y: 0 }, solver::GridPoint::Start);
    puzzle.set_grid_point(solver::Vec2 { x: 0, y: 1 }, solver::GridPoint::End);
    test_solve_puzzle(puzzle, 1);

    let mut puzzle = solver::Puzzle::new(0, 1);
    puzzle.set_grid_point(solver::Vec2 { x: 0, y: 1 }, solver::GridPoint::Start);
    puzzle.set_grid_point(solver::Vec2 { x: 0, y: 0 }, solver::GridPoint::End);
    test_solve_puzzle(puzzle, 1);

    let mut puzzle = solver::Puzzle::new(1, 0);
    puzzle.set_grid_point(solver::Vec2 { x: 0, y: 0 }, solver::GridPoint::Start);
    puzzle.set_grid_point(solver::Vec2 { x: 1, y: 0 }, solver::GridPoint::End);
    test_solve_puzzle(puzzle, 1);

    let mut puzzle = solver::Puzzle::new(1, 0);
    puzzle.set_grid_point(solver::Vec2 { x: 1, y: 0 }, solver::GridPoint::Start);
    puzzle.set_grid_point(solver::Vec2 { x: 0, y: 0 }, solver::GridPoint::End);
    test_solve_puzzle(puzzle, 1);
}

#[test]
fn puzzle_01() {
    let mut puzzle = solver::Puzzle::new(4, 4);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 1, y: 0 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 2, y: 0 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 3, y: 0 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 4, y: 0 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 0, y: 1 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 1, y: 2 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 3, y: 2 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 0, y: 3 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 4, y: 3 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 1, y: 1 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 3, y: 1 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 3, y: 2 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 0, y: 3 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 1, y: 4 }, solver::GridLine::Disconnected);
    puzzle.set_grid_point(solver::Vec2 { x: 0, y: 4 }, solver::GridPoint::Start);
    puzzle.set_grid_point(solver::Vec2 { x: 4, y: 0 }, solver::GridPoint::End);
    test_solve_puzzle(puzzle, 1);
}

#[test]
fn puzzle_02() {
    let mut puzzle = solver::Puzzle::new(6, 6);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 0, y: 3 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 0, y: 4 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 1, y: 0 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 1, y: 5 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 2, y: 1 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 2, y: 4 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 3, y: 1 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 3, y: 2 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 3, y: 3 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 3, y: 5 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 4, y: 1 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 4, y: 3 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 4, y: 5 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 5, y: 1 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 5, y: 2 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 5, y: 5 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 6, y: 4 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 2, y: 0 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 4, y: 0 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 0, y: 1 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 1, y: 1 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 3, y: 1 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 5, y: 1 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 0, y: 2 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 2, y: 2 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 0, y: 3 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 1, y: 3 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 4, y: 3 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 5, y: 3 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 1, y: 4 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 5, y: 4 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 0, y: 5 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 1, y: 5 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 2, y: 5 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 4, y: 5 }, solver::GridLine::Disconnected);
    puzzle.set_grid_point(solver::Vec2 { x: 4, y: 4 }, solver::GridPoint::Start);
    puzzle.set_grid_point(solver::Vec2 { x: 2, y: 0 }, solver::GridPoint::End);
    test_solve_puzzle(puzzle, 2);
}

#[test]
fn puzzle_03() {
    let mut puzzle = solver::Puzzle::new(10, 10);

    puzzle.set_grid_line_horizontal(Vec2 { y: 0, x: 1 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 0, x: 3 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 0, x: 6 }, GridLine::Disconnected);

    puzzle.set_grid_line_vertical(Vec2 { y: 0, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 0, x: 5 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 0, x: 7 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 0, x: 8 }, GridLine::Disconnected);


    puzzle.set_grid_line_horizontal(Vec2 { y: 1, x: 0 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 1, x: 3 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 1, x: 8 }, GridLine::Disconnected);

    puzzle.set_grid_line_vertical(Vec2 { y: 1, x: 1 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 1, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 1, x: 5 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 1, x: 6 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 1, x: 9 }, GridLine::Disconnected);


    puzzle.set_grid_line_horizontal(Vec2 { y: 2, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 2, x: 5 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 2, x: 6 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 2, x: 7 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 2, x: 9 }, GridLine::Disconnected);

    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 0 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 1 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 3 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 4 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 7 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 8 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 9 }, GridLine::Disconnected);


    puzzle.set_grid_line_horizontal(Vec2 { y: 3, x: 4 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 3, x: 5 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 3, x: 8 }, GridLine::Disconnected);

    puzzle.set_grid_line_vertical(Vec2 { y: 3, x: 1 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 3, x: 3 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 3, x: 4 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 3, x: 7 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 3, x: 9 }, GridLine::Disconnected);


    puzzle.set_grid_line_horizontal(Vec2 { y: 4, x: 1 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 4, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 4, x: 4 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 4, x: 5 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 4, x: 6 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 4, x: 9 }, GridLine::Disconnected);

    puzzle.set_grid_line_vertical(Vec2 { y: 4, x: 0 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 4, x: 1 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 4, x: 4 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 4, x: 5 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 4, x: 7 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 4, x: 8 }, GridLine::Disconnected);


    puzzle.set_grid_line_horizontal(Vec2 { y: 5, x: 0 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 5, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 5, x: 3 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 5, x: 5 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 5, x: 6 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 5, x: 8 }, GridLine::Disconnected);

    puzzle.set_grid_line_vertical(Vec2 { y: 5, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 5, x: 5 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 5, x: 10 }, GridLine::Disconnected);


    puzzle.set_grid_line_horizontal(Vec2 { y: 6, x: 0 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 6, x: 1 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 6, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 6, x: 5 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 6, x: 7 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 6, x: 8 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 6, x: 9 }, GridLine::Disconnected);

    puzzle.set_grid_line_vertical(Vec2 { y: 6, x: 4 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 6, x: 7 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 6, x: 9 }, GridLine::Disconnected);


    puzzle.set_grid_line_horizontal(Vec2 { y: 7, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 7, x: 3 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 7, x: 7 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 7, x: 8 }, GridLine::Disconnected);

    puzzle.set_grid_line_vertical(Vec2 { y: 7, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 7, x: 3 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 7, x: 5 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 7, x: 10 }, GridLine::Disconnected);


    puzzle.set_grid_line_horizontal(Vec2 { y: 8, x: 1 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 8, x: 4 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 8, x: 5 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 8, x: 6 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 8, x: 7 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 8, x: 9 }, GridLine::Disconnected);

    puzzle.set_grid_line_vertical(Vec2 { y: 8, x: 0 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 8, x: 4 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 8, x: 8 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 8, x: 9 }, GridLine::Disconnected);


    puzzle.set_grid_line_horizontal(Vec2 { y: 9, x: 1 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 9, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 9, x: 4 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 9, x: 6 }, GridLine::Disconnected);

    puzzle.set_grid_line_vertical(Vec2 { y: 9, x: 1 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 9, x: 6 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 9, x: 7 }, GridLine::Disconnected);
    puzzle.set_grid_line_vertical(Vec2 { y: 9, x: 9 }, GridLine::Disconnected);


    puzzle.set_grid_line_horizontal(Vec2 { y: 10, x: 2 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 10, x: 5 }, GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(Vec2 { y: 10, x: 8 }, GridLine::Disconnected);

    puzzle.set_grid_point(Vec2 { y: 10, x: 0 }, Start);
    puzzle.set_grid_point(Vec2 { y: 0, x: 10 }, End);

    test_solve_puzzle(puzzle, 6);
}

#[test]
fn puzzle_04() {
    let mut puzzle = solver::Puzzle::new(6, 6);

    puzzle.set_grid_line_horizontal(Vec2 { y: 0, x: 0 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 0, x: 2 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 0, x: 3 }, GridLine::Blocked);

    puzzle.set_grid_line_horizontal(Vec2 { y: 1, x: 0 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 1, x: 3 }, GridLine::Blocked);

    puzzle.set_grid_line_horizontal(Vec2 { y: 2, x: 0 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 2, x: 2 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 2, x: 4 }, GridLine::Blocked);

    puzzle.set_grid_line_horizontal(Vec2 { y: 3, x: 1 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 3, x: 3 }, GridLine::Blocked);

    puzzle.set_grid_line_horizontal(Vec2 { y: 4, x: 3 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 4, x: 4 }, GridLine::Blocked);

    puzzle.set_grid_line_horizontal(Vec2 { y: 5, x: 1 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 5, x: 4 }, GridLine::Blocked);

    puzzle.set_grid_line_horizontal(Vec2 { y: 6, x: 2 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 6, x: 4 }, GridLine::Blocked);


    puzzle.set_grid_line_vertical(Vec2 { y: 0, x: 4 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 0, x: 5 }, GridLine::Blocked);

    puzzle.set_grid_line_vertical(Vec2 { y: 1, x: 2 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 1, x: 3 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 1, x: 6 }, GridLine::Blocked);

    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 1 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 4 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 5 }, GridLine::Blocked);

    puzzle.set_grid_line_vertical(Vec2 { y: 3, x: 2 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 3, x: 3 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 3, x: 6 }, GridLine::Blocked);

    puzzle.set_grid_line_vertical(Vec2 { y: 4, x: 1 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 4, x: 4 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 4, x: 5 }, GridLine::Blocked);


    puzzle.set_grid_point(Vec2 { y: 6, x: 0 }, Start);
    puzzle.set_grid_point(Vec2 { y: 6, x: 6 }, Start);
    puzzle.set_grid_point(Vec2 { y: 0, x: 3 }, End);
    test_solve_puzzle(puzzle, 4);
}

#[test]
fn puzzle_05() {
    let mut puzzle = solver::Puzzle::new(6, 6);

    puzzle.set_grid_line_horizontal(Vec2 { y: 0, x: 3 }, GridLine::Blocked);

    puzzle.set_grid_line_horizontal(Vec2 { y: 1, x: 1 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 1, x: 2 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 1, x: 4 }, GridLine::Blocked);

    puzzle.set_grid_line_horizontal(Vec2 { y: 2, x: 2 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 2, x: 4 }, GridLine::Blocked);

    puzzle.set_grid_line_horizontal(Vec2 { y: 3, x: 1 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 3, x: 3 }, GridLine::Blocked);

    puzzle.set_grid_line_horizontal(Vec2 { y: 4, x: 3 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 4, x: 4 }, GridLine::Blocked);

    puzzle.set_grid_line_horizontal(Vec2 { y: 5, x: 1 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 5, x: 4 }, GridLine::Blocked);

    puzzle.set_grid_line_horizontal(Vec2 { y: 6, x: 2 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 6, x: 4 }, GridLine::Blocked);


    puzzle.set_grid_line_vertical(Vec2 { y: 0, x: 3 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 0, x: 5 }, GridLine::Blocked);

    puzzle.set_grid_line_vertical(Vec2 { y: 1, x: 2 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 1, x: 6 }, GridLine::Blocked);

    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 0 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 4 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 5 }, GridLine::Blocked);

    puzzle.set_grid_line_vertical(Vec2 { y: 3, x: 2 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 3, x: 3 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 3, x: 6 }, GridLine::Blocked);

    puzzle.set_grid_line_vertical(Vec2 { y: 4, x: 1 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 4, x: 4 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 4, x: 5 }, GridLine::Blocked);


    puzzle.set_grid_point(Vec2 { y: 6, x: 0 }, Start);
    puzzle.set_grid_point(Vec2 { y: 0, x: 6 }, End);
    puzzle.set_grid_point(Vec2 { y: 6, x: 6 }, End);

    test_solve_puzzle(puzzle, 72);
}

#[test]
fn puzzle_06() {
    let mut puzzle = solver::Puzzle::new(4, 4);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 0, y: 2 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 0, y: 3 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 1, y: 3 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 3, y: 0 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 4, y: 0 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_vertical(solver::Vec2 { x: 4, y: 1 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 2, y: 0 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 3, y: 0 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 3, y: 1 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 0, y: 3 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 0, y: 4 }, solver::GridLine::Disconnected);
    puzzle.set_grid_line_horizontal(solver::Vec2 { x: 1, y: 4 }, solver::GridLine::Disconnected);
    puzzle.set_grid_point(solver::Vec2 { x: 0, y: 0 }, solver::GridPoint::Start);
    puzzle.set_grid_point(solver::Vec2 { x: 4, y: 4 }, solver::GridPoint::End);
    test_solve_puzzle(puzzle, 344);
}