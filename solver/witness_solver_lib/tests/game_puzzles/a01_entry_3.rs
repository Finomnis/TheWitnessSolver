use crate::common::test_solve_puzzle;
use witness_solver_lib as solver;
use witness_solver_lib::Cell::*;
use witness_solver_lib::GridPoint::*;
use witness_solver_lib::Color::*;
use witness_solver_lib::Vec2;


#[test]
fn puzzle_01() {
    let mut puzzle = solver::Puzzle::new(1, 2);
    puzzle.set_cell(Vec2 { x: 0, y: 0 }, Square(Black));
    puzzle.set_cell(Vec2 { x: 0, y: 1 }, Square(White));
    puzzle.set_grid_point(Vec2 { x: 0, y: 1 }, Start);
    puzzle.set_grid_point(Vec2 { x: 1, y: 1 }, End);
    test_solve_puzzle(puzzle, 1);
}

#[test]
fn puzzle_02() {
    let mut puzzle = solver::Puzzle::new(1, 2);
    puzzle.set_cell(Vec2 { x: 0, y: 0 }, Square(Black));
    puzzle.set_cell(Vec2 { x: 0, y: 1 }, Square(White));
    puzzle.set_grid_point(Vec2 { x: 0, y: 2 }, Start);
    puzzle.set_grid_point(Vec2 { x: 1, y: 0 }, End);
    test_solve_puzzle(puzzle, 2);
}

#[test]
fn puzzle_03() {
    let mut puzzle = solver::Puzzle::new(1, 3);
    puzzle.set_cell(Vec2 { x: 0, y: 0 }, Square(Black));
    puzzle.set_cell(Vec2 { x: 0, y: 1 }, Square(Black));
    puzzle.set_cell(Vec2 { x: 0, y: 2 }, Square(White));
    puzzle.set_grid_point(Vec2 { x: 0, y: 3 }, Start);
    puzzle.set_grid_point(Vec2 { x: 1, y: 0 }, End);
    test_solve_puzzle(puzzle, 4);
}

#[test]
fn puzzle_04() {
    let mut puzzle = solver::Puzzle::new(2, 2);
    puzzle.set_cell(Vec2 { x: 0, y: 0 }, Square(Black));
    puzzle.set_cell(Vec2 { x: 0, y: 1 }, Square(Black));
    puzzle.set_cell(Vec2 { x: 1, y: 0 }, Square(Black));
    puzzle.set_cell(Vec2 { x: 1, y: 1 }, Square(White));
    puzzle.set_grid_point(Vec2 { x: 0, y: 2 }, Start);
    puzzle.set_grid_point(Vec2 { x: 2, y: 0 }, End);
    test_solve_puzzle(puzzle, 1);
}

#[test]
fn puzzle_05() {
    let mut puzzle = solver::Puzzle::new(3, 3);
    puzzle.set_cell(Vec2 { y: 0, x: 0 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 0, x: 1 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 0, x: 2 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 1, x: 0 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 1, x: 1 }, Square(White));
    puzzle.set_cell(Vec2 { y: 1, x: 2 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 2, x: 0 }, Square(White));
    puzzle.set_cell(Vec2 { y: 2, x: 1 }, Square(White));
    puzzle.set_cell(Vec2 { y: 2, x: 2 }, Square(White));
    puzzle.set_grid_point(Vec2 { y: 3, x: 0 }, Start);
    puzzle.set_grid_point(Vec2 { y: 0, x: 3 }, End);
    test_solve_puzzle(puzzle, 2);
}

#[test]
fn puzzle_06() {
    let mut puzzle = solver::Puzzle::new(3, 3);
    puzzle.set_cell(Vec2 { y: 0, x: 0 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 0, x: 1 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 0, x: 2 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 1, x: 0 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 1, x: 1 }, Square(White));
    puzzle.set_cell(Vec2 { y: 1, x: 2 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 2, x: 0 }, Square(White));
    puzzle.set_cell(Vec2 { y: 2, x: 1 }, Square(White));
    puzzle.set_cell(Vec2 { y: 2, x: 2 }, Square(White));
    puzzle.set_grid_point(Vec2 { y: 3, x: 0 }, Start);
    puzzle.set_grid_point(Vec2 { y: 2, x: 0 }, End);
    test_solve_puzzle(puzzle, 1);
}

#[test]
fn puzzle_07() {
    let mut puzzle = solver::Puzzle::new(4, 4);
    puzzle.set_cell(Vec2 { y: 0, x: 0 }, Square(Black));
    //puzzle.set_cell(Vec2 { y: 0, x: 1 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 0, x: 2 }, Square(White));
    puzzle.set_cell(Vec2 { y: 0, x: 3 }, Square(Black));

    puzzle.set_cell(Vec2 { y: 1, x: 0 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 1, x: 1 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 1, x: 2 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 1, x: 3 }, Square(Black));

    puzzle.set_cell(Vec2 { y: 2, x: 0 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 2, x: 1 }, Square(White));
    puzzle.set_cell(Vec2 { y: 2, x: 2 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 2, x: 3 }, Square(Black));

    puzzle.set_cell(Vec2 { y: 3, x: 0 }, Square(White));
    puzzle.set_cell(Vec2 { y: 3, x: 1 }, Square(White));
    puzzle.set_cell(Vec2 { y: 3, x: 2 }, Square(White));
    puzzle.set_cell(Vec2 { y: 3, x: 3 }, Square(Black));
    puzzle.set_grid_point(Vec2 { y: 4, x: 0 }, Start);
    puzzle.set_grid_point(Vec2 { y: 0, x: 1 }, End);
    test_solve_puzzle(puzzle, 2);
}

#[test]
fn puzzle_08() {
    let mut puzzle = solver::Puzzle::new(4, 4);
    puzzle.set_cell(Vec2 { y: 0, x: 0 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 0, x: 1 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 0, x: 2 }, Square(White));
    puzzle.set_cell(Vec2 { y: 0, x: 3 }, Square(Black));

    //puzzle.set_cell(Vec2 { y: 1, x: 0 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 1, x: 1 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 1, x: 2 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 1, x: 3 }, Square(Black));

    puzzle.set_cell(Vec2 { y: 2, x: 0 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 2, x: 1 }, Square(White));
    puzzle.set_cell(Vec2 { y: 2, x: 2 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 2, x: 3 }, Square(Black));

    puzzle.set_cell(Vec2 { y: 3, x: 0 }, Square(White));
    puzzle.set_cell(Vec2 { y: 3, x: 1 }, Square(White));
    puzzle.set_cell(Vec2 { y: 3, x: 2 }, Square(White));
    puzzle.set_cell(Vec2 { y: 3, x: 3 }, Square(Black));
    puzzle.set_grid_point(Vec2 { y: 4, x: 0 }, Start);
    puzzle.set_grid_point(Vec2 { y: 2, x: 4 }, End);
    test_solve_puzzle(puzzle, 2);
}

#[test]
fn puzzle_09() {
    let mut puzzle = solver::Puzzle::new(4, 4);
    //puzzle.set_cell(Vec2 { y: 0, x: 0 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 0, x: 1 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 0, x: 2 }, Square(White));
    puzzle.set_cell(Vec2 { y: 0, x: 3 }, Square(Black));

    puzzle.set_cell(Vec2 { y: 1, x: 0 }, Square(Black));
    //puzzle.set_cell(Vec2 { y: 1, x: 1 }, Square(Black));
    //puzzle.set_cell(Vec2 { y: 1, x: 2 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 1, x: 3 }, Square(Black));

    puzzle.set_cell(Vec2 { y: 2, x: 0 }, Square(Black));
    puzzle.set_cell(Vec2 { y: 2, x: 1 }, Square(White));
    puzzle.set_cell(Vec2 { y: 2, x: 2 }, Square(Black));
    //puzzle.set_cell(Vec2 { y: 2, x: 3 }, Square(Black));

    puzzle.set_cell(Vec2 { y: 3, x: 0 }, Square(White));
    puzzle.set_cell(Vec2 { y: 3, x: 1 }, Square(White));
    puzzle.set_cell(Vec2 { y: 3, x: 2 }, Square(White));
    puzzle.set_cell(Vec2 { y: 3, x: 3 }, Square(Black));
    puzzle.set_grid_point(Vec2 { y: 4, x: 0 }, Start);
    puzzle.set_grid_point(Vec2 { y: 4, x: 3 }, End);
    test_solve_puzzle(puzzle, 1);
}
