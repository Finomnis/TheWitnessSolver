use witness_solver_lib as solver;
use std::time::Duration;

pub fn test_solve_puzzle(puzzle: solver::Puzzle, correct_solutions_count: usize) {
    let mut solutions_counter = 0;

    let mut callback_solution = |_: &solver::Solution, _: usize| {
        solutions_counter += 1;
    };

    let mut solver = solver::Solver::new(puzzle, &mut callback_solution, &|_, _| {}, Duration::from_secs(100));
    solver.run();

    assert_eq!(solutions_counter, correct_solutions_count);
}
