use witness_solver_lib::{Puzzle, GridPoint, GridLine, Cell, Solution, Direction, Color, Vec2};

#[derive(Clone, Debug)]
pub struct LineConfiguration { left: bool, right: bool, up: bool, down: bool }

#[derive(Clone, Debug)]
pub struct GridElement {
    thin_line: LineConfiguration,
    thick_line: LineConfiguration,
    symbol: Option<String>,
}

#[derive(Clone, Debug)]
pub struct Grid {
    width: usize,
    height: usize,
    data: Vec<GridElement>,
}

fn colorize(str_in: &str, col: Color) -> String {
    let col_code = match col {
        Color::Black => "\x1b[38;2;65;65;65m",
        Color::Red => "\x1b[31m",
        Color::Green => "\x1b[32m",
        Color::Yellow => "\x1b[33m",
        Color::Blue => "\x1b[34m",
        Color::Magenta => "\x1b[35m",
        Color::Cyan => "\x1b[36m",
        Color::White => "\x1b[38;5;15m",
    };

    col_code.to_string() + str_in + "\x1b[0m"
}

fn get_ascii_line(thin: &LineConfiguration, thick: &LineConfiguration) -> &'static str {
    fn get_ascii_line_thin(thin: &LineConfiguration, thick: &LineConfiguration) -> &'static str {
        match thin {
            LineConfiguration { left: true, right: true, up: true, down: true } => if thick.left { "═┼" } else { "─┼" },
            LineConfiguration { left: false, right: true, up: true, down: true } => if thick.left { "═├" } else { " ├" },
            LineConfiguration { left: true, right: false, up: true, down: true } => if thick.left { "═┤" } else { "─┤" },
            LineConfiguration { left: true, right: true, up: false, down: true } => if thick.left { "═┬" } else { "─┬" },
            LineConfiguration { left: true, right: true, up: true, down: false } => if thick.left { "═┴" } else { "─┴" },
            LineConfiguration { left: false, right: false, up: true, down: true } => if thick.left { "═│" } else { " │" },
            LineConfiguration { left: false, right: true, up: false, down: true } => if thick.left { "═┌" } else { " ┌" },
            LineConfiguration { left: false, right: true, up: true, down: false } => if thick.left { "═└" } else { " └" },
            LineConfiguration { left: true, right: false, up: false, down: true } => if thick.left { "═┐" } else { "─┐" },
            LineConfiguration { left: true, right: false, up: true, down: false } => if thick.left { "═┘" } else { "─┘" },
            LineConfiguration { left: true, right: true, up: false, down: false } => if thick.left { "═─" } else { "──" },
            LineConfiguration { left: false, right: false, up: false, down: true } => if thick.left { "═ " } else { "  " },
            LineConfiguration { left: false, right: false, up: true, down: false } => if thick.left { "═ " } else { "  " },
            LineConfiguration { left: false, right: true, up: false, down: false } => if thick.left { "═ " } else { "  " },
            LineConfiguration { left: true, right: false, up: false, down: false } => if thick.left { "═ " } else { "─ " },
            LineConfiguration { left: false, right: false, up: false, down: false } => if thick.left { "═ " } else { "  " },
        }
    }

    match thick {
        LineConfiguration { left: true, right: true, up: true, down: true } => "═╬",
        LineConfiguration { left: false, right: true, up: true, down: true } => if thin.left { "─╠" } else { " ╠" },
        LineConfiguration { left: true, right: false, up: true, down: true } => "═╣",
        LineConfiguration { left: true, right: true, up: false, down: true } => "═╦",
        LineConfiguration { left: true, right: true, up: true, down: false } => "═╩",
        LineConfiguration { left: false, right: false, up: true, down: true } => if thin.left { if thin.right { "─╫" } else { "─║" } } else { " ║" },
        LineConfiguration { left: false, right: true, up: false, down: true } => if thin.left { "─╔" } else { " ╔" },
        LineConfiguration { left: false, right: true, up: true, down: false } => if thin.left { "─╚" } else { " ╚" },
        LineConfiguration { left: true, right: false, up: false, down: true } => "═╗",
        LineConfiguration { left: true, right: false, up: true, down: false } => "═╝",
        LineConfiguration { left: true, right: true, up: false, down: false } => if thin.up & &thin.down { "═╪" } else { "══" },
        LineConfiguration { left: false, right: false, up: false, down: true } => get_ascii_line_thin(thin, thick),
        LineConfiguration { left: false, right: false, up: true, down: false } => get_ascii_line_thin(thin, thick),
        LineConfiguration { left: false, right: true, up: false, down: false } => get_ascii_line_thin(thin, thick),
        LineConfiguration { left: true, right: false, up: false, down: false } => get_ascii_line_thin(thin, thick),
        LineConfiguration { left: false, right: false, up: false, down: false } => get_ascii_line_thin(thin, thick),
    }
}

impl Grid {
    pub fn new(width: usize, height: usize) -> Grid {
        Grid {
            width,
            height,
            data: vec![GridElement {
                thin_line: LineConfiguration { left: false, right: false, up: false, down: false },
                thick_line: LineConfiguration { left: false, right: false, up: false, down: false },
                symbol: None,
            }; width * height],
        }
    }

    fn get_offset(&self, pos: Vec2) -> usize {
        self.width * pos.y + pos.x
    }

    pub fn set_symbol(&mut self, pos: Vec2, val: &str) {
        let arr_pos = self.get_offset(pos);
        self.data[arr_pos].symbol = Some(val.to_string());
    }

    pub fn set_thin_line(&mut self, pos: Vec2, dir: Direction) {
        let arr_pos = self.get_offset(pos);
        let mut line_config = &mut self.data[arr_pos].thin_line;
        match dir {
            Direction::Left => line_config.left = true,
            Direction::Right => line_config.right = true,
            Direction::Up => line_config.up = true,
            Direction::Down => line_config.down = true,
        }
    }

    pub fn set_thick_line(&mut self, pos: Vec2, dir: Direction) {
        let arr_pos = self.get_offset(pos);
        let mut line_config = &mut self.data[arr_pos].thick_line;
        match dir {
            Direction::Left => line_config.left = true,
            Direction::Right => line_config.right = true,
            Direction::Up => line_config.up = true,
            Direction::Down => line_config.down = true,
        }
    }


    pub fn clear_thick_lines(&mut self) {
        for elem in &mut self.data {
            elem.thick_line.left = false;
            elem.thick_line.right = false;
            elem.thick_line.up = false;
            elem.thick_line.down = false;
        }
    }

    //pub fn get_width(&self) -> usize { self.width }
    //pub fn get_height(&self) -> usize { self.height }

    pub fn render_to_ascii(&self) -> String {
        let mut result = String::new();

        for (id, elem) in self.data.iter().enumerate() {
            if id % self.width == 0 && id > 0 {
                result += "\n";
            }
            if let Some(symbol) = &elem.symbol {
                let left_sign = if elem.thick_line.left { "═" } else if elem.thin_line.left { "─" } else { " " };
                result += left_sign;
                result += &symbol;
            } else {
                result += get_ascii_line(&elem.thin_line, &elem.thick_line);
            }
        }
        result
    }
}

pub trait Render {
    fn render_to_grid(&self, grid: Option<Grid>) -> Grid;
}


impl Render for Puzzle {
    fn render_to_grid(&self, grid_in: Option<Grid>) -> Grid {
        let grid_width = self.get_width() * 2 + 1;
        let grid_height = self.get_height() * 2 + 1;

        let mut grid = match grid_in {
            Some(g) => g,
            None => Grid::new(grid_width, grid_height)
        };

        assert_eq!(grid_width, grid.width);
        assert_eq!(grid_height, grid.height);

        // Horizontal lines
        for (y, line) in self.get_grid_lines_horizontal().iter().enumerate() {
            for (x, elem) in line.iter().enumerate() {
                match elem {
                    GridLine::Normal | GridLine::Dot | GridLine::Blocked => {
                        grid.set_thin_line(Vec2 { x: 2 * x + 1, y: 2 * y }, Direction::Right);
                        grid.set_thin_line(Vec2 { x: 2 * x + 1, y: 2 * y }, Direction::Left);
                        grid.set_thin_line(Vec2 { x: 2 * x, y: 2 * y }, Direction::Right);
                        grid.set_thin_line(Vec2 { x: 2 * x + 2, y: 2 * y }, Direction::Left);
                    }
                    GridLine::Disconnected => ()
                }

                // Draw special elements
                if let Some(msg) = {
                    match elem {
                        GridLine::Disconnected | GridLine::Normal => None,
                        GridLine::Dot => Some(colorize("●", Color::Black)),
                        GridLine::Blocked => Some(" ".to_string())
                    }
                } {
                    grid.set_symbol(Vec2 { x: 2 * x + 1, y: 2 * y }, &msg);
                }
            }
        }

        // Vertical lines
        for (y, line) in self.get_grid_lines_vertical().iter().enumerate() {
            for (x, elem) in line.iter().enumerate() {
                match elem {
                    GridLine::Normal | GridLine::Blocked | GridLine::Dot => {
                        grid.set_thin_line(Vec2 { x: 2 * x, y: 2 * y + 1 }, Direction::Down);
                        grid.set_thin_line(Vec2 { x: 2 * x, y: 2 * y + 1 }, Direction::Up);
                        grid.set_thin_line(Vec2 { x: 2 * x, y: 2 * y }, Direction::Down);
                        grid.set_thin_line(Vec2 { x: 2 * x, y: 2 * y + 2 }, Direction::Up);
                    }
                    GridLine::Disconnected => ()
                }

                // Draw special elements
                if let Some(msg) = {
                    match elem {
                        GridLine::Disconnected | GridLine::Normal => None,
                        GridLine::Dot => Some(colorize("●", Color::Black)),
                        GridLine::Blocked => Some(" ".to_string()),
                    }
                } {
                    grid.set_symbol(Vec2 { x: 2 * x, y: 2 * y + 1 }, &msg);
                }
            }
        }

        // Crossing points
        for (y, line) in self.get_grid_points().iter().enumerate() {
            for (x, elem) in line.iter().enumerate() {
                if let Some(msg) = {
                    match elem {
                        GridPoint::Normal => None,
                        GridPoint::Start => Some("O".to_string()),
                        GridPoint::End => Some("e".to_string()),
                        GridPoint::Dot => Some(colorize("●", Color::Black)),
                    }
                } {
                    grid.set_symbol(Vec2 { x: 2 * x, y: 2 * y }, &msg);
                }
            }
        }

        // Cell elements
        for (y, line) in self.get_cells().iter().enumerate() {
            for (x, elem) in line.iter().enumerate() {
                match elem {
                    Cell::Empty => (),
                    Cell::Square(col) => {
                        grid.set_symbol(Vec2 { x: 2 * x + 1, y: 2 * y + 1 }, &colorize("■", *col));
                    }
                }
            }
        }

        grid
    }
}

impl Render for Solution {
    fn render_to_grid(&self, grid_in: Option<Grid>) -> Grid {
        let size = self.get_size();

        let grid_width = size.x * 2 - 1;
        let grid_height = size.y * 2 - 1;

        let mut grid = match grid_in {
            Some(g) => g,
            None => Grid::new(grid_width, grid_height)
        };

        assert_eq!(grid_width, grid.width);
        assert_eq!(grid_height, grid.height);

        grid.clear_thick_lines();

        // Begin at start_pos, then move along the directions
        let mut previous_pos = self.get_start_pos();
        loop {
            if let Some(current_direction) = self.get(previous_pos) {
                // Compute next position
                let next_pos = match current_direction {
                    Direction::Left => Vec2 { x: previous_pos.x - 1, y: previous_pos.y },
                    Direction::Right => Vec2 { x: previous_pos.x + 1, y: previous_pos.y },
                    Direction::Up => Vec2 { x: previous_pos.x, y: previous_pos.y - 1 },
                    Direction::Down => Vec2 { x: previous_pos.x, y: previous_pos.y + 1 },
                };

                // Draw
                match current_direction {
                    Direction::Left | Direction::Right => {
                        grid.set_thick_line(Vec2 { x: previous_pos.x + next_pos.x, y: previous_pos.y + next_pos.y }, Direction::Left);
                        grid.set_thick_line(Vec2 { x: previous_pos.x + next_pos.x, y: previous_pos.y + next_pos.y }, Direction::Right);
                        grid.set_thick_line(Vec2 { x: previous_pos.x + next_pos.x + 1, y: previous_pos.y + next_pos.y }, Direction::Left);
                        grid.set_thick_line(Vec2 { x: previous_pos.x + next_pos.x - 1, y: previous_pos.y + next_pos.y }, Direction::Right);
                    }
                    Direction::Up | Direction::Down => {
                        grid.set_thick_line(Vec2 { x: previous_pos.x + next_pos.x, y: previous_pos.y + next_pos.y }, Direction::Up);
                        grid.set_thick_line(Vec2 { x: previous_pos.x + next_pos.x, y: previous_pos.y + next_pos.y }, Direction::Down);
                        grid.set_thick_line(Vec2 { x: previous_pos.x + next_pos.x, y: previous_pos.y + next_pos.y + 1 }, Direction::Up);
                        grid.set_thick_line(Vec2 { x: previous_pos.x + next_pos.x, y: previous_pos.y + next_pos.y - 1 }, Direction::Down);
                    }
                }

                previous_pos = next_pos;
            } else {
                break;
            }
        }

        grid
    }
}