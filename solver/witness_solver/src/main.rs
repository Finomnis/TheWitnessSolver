mod ascii_renderer;

use witness_solver_lib as solver;
use crate::ascii_renderer::Render;
use clicolors_control;

use std::time::Duration;
use std::iter;
use witness_solver_lib::GridPoint::*;
use witness_solver_lib::{Vec2, GridLine};

fn main() {
    clicolors_control::configure_terminal();

    let mut puzzle = solver::Puzzle::new(6, 6);

    puzzle.set_grid_line_horizontal(Vec2 { y: 0, x: 3 }, GridLine::Blocked);

    puzzle.set_grid_line_horizontal(Vec2 { y: 1, x: 1 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 1, x: 2 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 1, x: 4 }, GridLine::Blocked);

    puzzle.set_grid_line_horizontal(Vec2 { y: 2, x: 2 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 2, x: 4 }, GridLine::Blocked);

    puzzle.set_grid_line_horizontal(Vec2 { y: 3, x: 1 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 3, x: 3 }, GridLine::Blocked);

    puzzle.set_grid_line_horizontal(Vec2 { y: 4, x: 3 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 4, x: 4 }, GridLine::Blocked);

    puzzle.set_grid_line_horizontal(Vec2 { y: 5, x: 1 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 5, x: 4 }, GridLine::Blocked);

    puzzle.set_grid_line_horizontal(Vec2 { y: 6, x: 2 }, GridLine::Blocked);
    puzzle.set_grid_line_horizontal(Vec2 { y: 6, x: 4 }, GridLine::Blocked);


    puzzle.set_grid_line_vertical(Vec2 { y: 0, x: 3 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 0, x: 5 }, GridLine::Blocked);

    puzzle.set_grid_line_vertical(Vec2 { y: 1, x: 2 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 1, x: 6 }, GridLine::Blocked);

    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 0 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 4 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 2, x: 5 }, GridLine::Blocked);

    puzzle.set_grid_line_vertical(Vec2 { y: 3, x: 2 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 3, x: 3 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 3, x: 6 }, GridLine::Blocked);

    puzzle.set_grid_line_vertical(Vec2 { y: 4, x: 1 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 4, x: 4 }, GridLine::Blocked);
    puzzle.set_grid_line_vertical(Vec2 { y: 4, x: 5 }, GridLine::Blocked);


    puzzle.set_grid_point(Vec2 { y: 6, x: 0 }, Start);
    puzzle.set_grid_point(Vec2 { y: 0, x: 6 }, End);
    puzzle.set_grid_point(Vec2 { y: 6, x: 6 }, End);

    solve(puzzle);
}


fn move_up(num: usize) -> String {
    iter::repeat("\x1b[1A\x1b[K").take(num).collect::<Vec<&str>>().concat()
}

fn solve(puzzle: witness_solver_lib::Puzzle) {
    let render_grid = puzzle.render_to_grid(None);

    println!();
    println!("### Input ############");
    println!();
    println!("{}", render_grid.render_to_ascii());
    println!();
    println!();

    println!("### Solutions ########");
    println!();
    println!();
    println!("### Current State ####");
    println!();
    println!("{}", render_grid.render_to_ascii());
    println!();

    let mut solutions_counter = 0;

    let mut callback_solution = |solution: &solver::Solution, iterations: usize| {
        solutions_counter += 1;
        let grid = render_grid.clone();
        let grid = solution.render_to_grid(Some(grid));
        //std::thread::sleep(Duration::from_millis(500));
        println!("{}\nIterations: {}", move_up(2 * solution.get_size().y + 3) + &grid.render_to_ascii() + "\n\n\n### Current State ####\n\n" + &grid.render_to_ascii(), iterations);
        //std::thread::sleep(Duration::from_millis(500));
    };

    let callback_status = |status: &solver::Solution, iterations: usize| {
        let grid = render_grid.clone();
        let grid = status.render_to_grid(Some(grid));
        println!("{}\nIterations: {}", move_up(2 * status.get_size().y) + &grid.render_to_ascii(), iterations);
    };

    let mut solver = solver::Solver::new(puzzle, &mut callback_solution, &callback_status, Duration::from_millis(100));
    solver.run();

    println!();
    println!("Done. Solutions found: {}", solutions_counter);
}

